/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rabahis
 *
 * Created on December 8, 2017, 5:13 PM
 */

#include <cstdlib>
//#include "StdAfxRegArchLib.h"
#include "StdAfxRegArchLibChildren.h"

using namespace std;
using namespace RegArchLib;
/*
 * 
 */
int main(int argc, char** argv) {
 
    int t = 21;
    //Initialization for the ARMA gaussian model
    cDVector tmp(3);
    
    tmp[0] = 0.3;
    tmp[1] = 0.78;
    tmp[2] = 0.36;   
    cAr ar(tmp);
    
    tmp[0] = 0.43;
    tmp[1] = 0.38;
    tmp[2] = 0.86;
    cMa ma(tmp);
    
    cConst constArma(1);
    constArma.Set(4,0);

    tmp.Delete();
    
    //Build an ARMA model
    cCondMean mean(3);
    mean.Set(&constArma, 0);
    mean.Set(&ar, 1);
    mean.Set(&ma, 2);
    
    //Vectors y, u, h, eps, x which will be used to create the cRegArchValue
    cDVector y(t);
    cDVector u(t);
    cDVector h(t);
    cDVector eps(t);
    cDVector m(t);  
    cDMatrix x(t,t);
    
    //Initialization of the vector y
    for(int i = 0; i<(t-1); i++){
        y[i] = i;
    }
    
    //Define u(0)
    u[0] = -constArma.Get(0);
    
    //Create a cRegArchValue
    cRegArchValue value(y, u, h, eps, m, x);
    
    //Fill the vector u
    for(int i = 1; i<(t-1); i++){
        u[i]= y[i] - constArma.Get(0) - ar.mComputeMean(value,i) - 
                ma.mComputeMean(value,i);
    }
    //Conditional expectation of the ARMA model
    double espCond = mean.mComputeMean(value, t);
    
    cout<<"L'espérance conditionnelle du modèle ARMA gaussien en t = " << t << 
            " est de : " << espCond << ".\n";
    
    return 0;
}


