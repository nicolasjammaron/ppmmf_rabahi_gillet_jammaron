/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   StdAfxRegArchLib2.h
 * Author: rabahis
 *
 * Created on December 12, 2017, 7:27 PM
 */

#ifndef STDAFXREGARCHLIB2_H
#define STDAFXREGARCHLIB2_H

#include "cConst.h"
#include "cAr.h"
#include "cMa.h"

#include "cConstCondVar.h"
#include "cArch.h"
#include "cGarch.h"

#include "cNormResiduals.h"
#include "cStudentResiduals.h"

#include "cCondMean.h"

#endif /* STDAFXREGARCHLIB2_H */

