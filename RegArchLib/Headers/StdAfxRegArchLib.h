/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   StdAfxRegArchLib.h
 * Author: gillemax
 *
 * Created on December 4, 2017, 12:20 PM
 */


#pragma once

#ifndef _STDAFXREGARCHLIB_H_
#define _STDAFXREGARCHLIB_H_

//#include <cstdlib>
//#include <cstring>
#include <cmath>
#include <iostream>

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h> 

#include "StdAfxError.h"
#include "StdAfxVectorAndMatrix.h"

#include "RegArchDef.h"
/*#include "cAbstCondMean.h"
#include "cAbstCondVar.h"
#include "cAbstResiduals.h"
*/
/*
#include "cConst.h"
#include "cAr.h"
#include "cMa.h"

#include "cConstCondVar.h"
#include "cArch.h"
#include "cGarch.h"

#include "cNormResiduals.h"
#include "cStudentResiduals.h"

#include "cCondMean.h"
#include "cRegArchValue.h"
#include "cRegArchModel.h"*/

#define WIN32_LEAN_AND_MEAN             // Exclure les en-t�tes Windows rarement utilis�s


// TODO: faites r�f�rence ici aux en-t�tes suppl�mentaires n�cessaires au programme




#endif /* STDAFXREGARCHLIB_H */

