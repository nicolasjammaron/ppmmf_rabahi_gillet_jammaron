/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   cRegArchValue.h
 * Author: rabahis
 *
 * Created on December 4, 2017, 11:55 AM
 */
#pragma once
#ifndef CREGARCHVALUE_H
#define CREGARCHVALUE_H

#include "StdAfxRegArchLib.h"
//#include "RegArchDef.h"

namespace RegArchLib {
    class cRegArchValue
    {
    private :
        cDVector mvYt;
        cDVector mvUt;
        cDVector mvHt;
        cDVector mvEpst;
        cDVector mvMt;
        cDMatrix mvXt;
        
    public:
        cRegArchValue(uint theSize, cDMatrix* theXt=NULL); //A default constructor
        cRegArchValue(const cDVector& theYt,const cDVector& theUt,const 
                cDVector& theHt,const cDVector& theEpst, const cDVector& theMt, const cDMatrix& theXt);  // An other constructor
        ~cRegArchValue(); //A simple destructor
        //Getter and Setter
        cDVector getYt() const; 
        cDVector getUt() const;
        cDVector getHt() const;
        cDVector getEpst() const;
        cDVector getMt() const;
        double getYt(int theIndex) const; 
        double getUt(int theIndex) const;
        double getHt(int theIndex) const;
        double getEpst(int theIndex) const;
        double getMt(int theIndex) const;
        void setYt(const cDVector& theYt);
        void setUt(const cDVector& theUt);
        void setHt(const cDVector& theHt);
        void setEpst(const cDVector& theEpst);
        void setMt(const cDVector& theMt);
        void setYt(double theValue, int theIndex=0);
        void setUt(double theValue, int theIndex=0);
        void setHt(double theValue, int theIndex=0);
        void setEpst(double theValue, int theIndex=0);
        void setMt(double theValue, int theIndex=0);
        
        void ReAlloc(uint theSize);
        
    };
}


#endif /* CREGARCHVALUE_H */

