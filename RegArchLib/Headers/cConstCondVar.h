/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   cConstCondVar.h
 * Author: gillemax
 *
 * Created on December 4, 2017, 11:54 AM
 */
#pragma once
#ifndef CCONSTCONDVAR_H
#define CCONSTCONDVAR_H

//#include "StdAfxRegArchLib.h"
#include "cAbstCondVar.h"
//#include "cRegArchValue.h"


namespace RegArchLib {
    class cConstCondVar : public cAbstCondVar
    {
    public :
        cConstCondVar(uint theNParams = 0);
        cConstCondVar(const cDVector& theParams);
        ~cConstCondVar();
        double mComputeVar(const cRegArchValue & theData, int t) const;
    };
}

#endif /* CCONSTCONDVAR_H */

