/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   cGarch.h
 * Author: gillemax
 *
 * Created on December 4, 2017, 11:54 AM
 */
#pragma once
#ifndef CGARCH_H
#define CGARCH_H

//#include "StdAfxRegArchLib.h"
#include "cAbstCondVar.h"
//#include "cRegArchValue.h"

namespace RegArchLib {
    class cGarch : public cAbstCondVar
    {
        cGarch(uint theNParams = 0);
        cGarch(const cDVector& theParams);
        ~cGarch();
        double mComputeVar(const cRegArchValue & theData, int t) const;
    };
}

#endif /* CGARCH_H */

