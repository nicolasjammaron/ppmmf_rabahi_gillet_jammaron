/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   cStudentResiduals.h
 * Author: rabahis
 *
 * Created on December 4, 2017, 11:54 AM
 */
#pragma once
#ifndef CSTUDENTRESIDUALS_H
#define CSTUDENTRESIDUALS_H

//#include "StdAfxRegArchLib.h"
#include "cAbstResiduals.h"
//#include "cRegArchValue.h"
 namespace RegArchLib {
    class cStudentResiduals : public cAbstResiduals
    {
    public:
        cStudentResiduals(int theNbParam=0); //A default constructor
        //cStudentResiduals(int theNbParam, double theValue); // An other constructor
        cStudentResiduals(const cDVector& theParams); // An other constructor
        ~cStudentResiduals(); //A simple destructor
        double mLogDensity(double d);
        double mSimulate();
    };
}

#endif /* CSTUDENTRESIDUALS_H */

