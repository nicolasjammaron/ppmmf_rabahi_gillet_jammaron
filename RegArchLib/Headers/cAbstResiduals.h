/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   cAbstResiduals.h
 * Author: rabahis
 *
 * Created on December 4, 2017, 11:47 AM
 */
#pragma once
#ifndef CABSTRESIDUALS_H
#define CABSTRESIDUALS_H

#include "StdAfxRegArchLib.h"
/*#include "RegArchDef.h"
#include <gsl/gsl_randist.h>
#include <ctime>
#include <cmath>
#include "cRegArchValue.h"*/


namespace RegArchLib {
    class cAbstResiduals
    {
    protected :
        int mtNbParam;
        cDVector  mtParam;
    public:
        cAbstResiduals(int theNbParam=0); // An other constructor
        cAbstResiduals(const cDVector & theParams); // An other constructor
        ~cAbstResiduals(); //A simple destructor
        void Set(int theIndex, double theValue); // Set the value for an index in the vector
        void Set(int theNbParam); // Set the number of parameters
        void Set(const cDVector & theParam);
        double Get(int theIndex); // Get
        virtual double mLogDensity(double theValue) =0; // Calculate the log density 
        virtual double mSimulate()=0; //Simulate
    };
}

#endif /* CABSTRESIDUALS_H */

