/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   cAbstCondVar.h
 * Author: gillemax
 *
 * Created on December 4, 2017, 11:54 AM
 */
#pragma once
#ifndef CABSTCONDVAR_H
#define CABSTCONDVAR_H

#include "StdAfxRegArchLib.h"
//#include "RegArchDef.h"
#include "cRegArchValue.h"

using namespace std;

namespace RegArchLib {
    
    class cAbstCondVar
    {
    protected :
        int mtNParams;
        cDVector mtParams;
    public :
        cAbstCondVar(uint theNParams = 0);
        cAbstCondVar(const cDVector& theParams);
        virtual ~cAbstCondVar();
        void Delete(void);
        void Print(ostream& theOut=cout) const;
        void ReAlloc(uint theSize, uint theNumParam = 0);
	void ReAlloc(const cDVector& theVectParam);
	void Set(double theValue, uint theIndex = 0);
	void Set(const cDVector& theVectParam);
	double Get(uint theIndex = 0);
	uint GetNParam(void) const;
        virtual double mComputeVar(const cRegArchValue & theData, int t) const = 0;
    } ;
    
}

#endif /* CABSTCONDVAR_H */

