/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   cAbstCondMean.h
 * Author: gillemax
 *
 * Created on December 4, 2017, 11:52 AM
 */
#pragma once
#ifndef CABSTCONDMEAN_H
#define CABSTCONDMEAN_H

#include "StdAfxRegArchLib.h"
#include "cRegArchValue.h"

using namespace std;

namespace RegArchLib {
    
    class cAbstCondMean
    {
    protected :
        int mtNParams;
        cDVector mtParams;
    public :
        cAbstCondMean(uint theNParams = 0);
        cAbstCondMean(const cDVector& theParams);
        virtual ~cAbstCondMean();
        void Delete(void);
//      void Print(ostream& theOut=cout) const;
        void ReAlloc(uint theSize = 0);
	void ReAlloc(const cDVector& theVectParam);
	void Set(double theValue, uint theIndex = 0);
	void Set(const cDVector& theVectParam);
	double Get(uint theIndex = 0);
	int GetNParam(void) const;
        virtual double mComputeMean(const cRegArchValue & theData, int t) const = 0;
    } ;
}



#endif /* CABSTCONDMEAN_H */

