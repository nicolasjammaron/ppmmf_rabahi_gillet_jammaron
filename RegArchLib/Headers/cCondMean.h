/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   cCondMean.h
 * Author: rabahis
 *
 * Created on December 4, 2017, 11:54 AM
 */
#pragma once 
#ifndef CCONDMEAN_H
#define CCONDMEAN_H

#include "StdAfxRegArchLib.h"
#include "cAbstCondMean.h"
#include "cRegArchValue.h"

#include <iostream>
//using namespace std;

#include <vector>
/*
#include "RegArchDef.h"
#include "cAbstCondMean.h"
#include "cRegArchValue.h"*/

namespace RegArchLib {
    class cCondMean
    {
    private :
        std::vector<cAbstCondMean*> mvMeans;
    public:
        cCondMean(uint theNMean = 0);
        cCondMean(const std::vector<cAbstCondMean *> & theMeans);
        virtual ~cCondMean();
	void Print(ostream& theOut = cout) const; 
	void Set(cAbstCondMean *theMean, uint theIndex = 0);
	void Set(const std::vector<cAbstCondMean *>& theVectParam);
	cAbstCondMean * Get(uint theIndex = 0) const;
	uint GetNMean(void) const;
        double mComputeMean(const cRegArchValue & theData, int t) const;
    };
}


#endif /* CCONDMEAN_H */

