/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   cMa.h
 * Author: gillemax
 *
 * Created on December 4, 2017, 11:53 AM
 */
#pragma once
#ifndef CMA_H
#define CMA_H

//#include "StdAfxRegArchLib.h"
#include "cAbstCondMean.h"
//#include "cRegArchValue.h"

namespace RegArchLib {
    class cMa : public cAbstCondMean
    {
    public:
        cMa(uint theNParams = 0);
        cMa(const cDVector& theParams);
        ~cMa();
        double mComputeMean(const cRegArchValue & theData, int t) const;
    };
}

#endif /* CMA_H */

