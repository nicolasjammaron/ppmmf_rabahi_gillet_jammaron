/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   cConst.h
 * Author: gillemax
 *
 * Created on December 4, 2017, 11:53 AM
 */
#pragma once
#ifndef CCONST_H
#define CCONST_H

//#include "StdAfxRegArchLib.h"

#include "cAbstCondMean.h"
//#include "cRegArchValue.h"

namespace RegArchLib {
    class cConst : public cAbstCondMean
    {
    public:
        cConst(uint theNParams = 0);
        cConst(const cDVector& theParams);
        ~cConst();
        double mComputeMean(const cRegArchValue & theData, int t) const;
    };
}

#endif /* CCONST_H */

