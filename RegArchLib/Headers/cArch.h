/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   cArch.h
 * Author: gillemax
 *
 * Created on December 4, 2017, 11:54 AM
 */
#pragma once
#ifndef CARCH_H
#define CARCH_H

//#include "StdAfxRegArchLib.h"

#include "cAbstCondVar.h"

namespace RegArchLib {
    class cArch : public cAbstCondVar
    {
        cArch(uint theNParams = 0);
        cArch(const cDVector& theParams);
        ~cArch();
        double mComputeVar(const cRegArchValue & theData, int t) const;
    };
}

#endif /* CARCH_H */

