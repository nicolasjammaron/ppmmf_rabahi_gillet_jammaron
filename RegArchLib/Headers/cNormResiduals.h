/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   cNormResiduals.h
 * Author: rabahis
 *
 * Created on December 4, 2017, 11:53 AM
 */
#pragma once
#ifndef CNORMRESIDUALS_H
#define CNORMRESIDUALS_H

//#include "StdAfxRegArchLib.h"
#include "cAbstResiduals.h"
//#include "cRegArchValue.h"

 namespace RegArchLib {
    class cNormResiduals : public cAbstResiduals
    {
    public:
        cNormResiduals(int theNbParam=0); //A default constructor
        //cNormResiduals(int theNbParam, double theValue); // An other constructor
        cNormResiduals(const cDVector& theParams); // An other constructor
        ~cNormResiduals(); //A simple destructor
        double mLogDensity(double d);
        double mSimulate();
    };
}


#endif /* CNORMRESIDUALS_H */

