/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   cAr.h
 * Author: gillemax
 *
 * Created on December 4, 2017, 11:53 AM
 */
#pragma once 
#ifndef CAR_H
#define CAR_H

//#include "StdAfxRegArchLib.h"
#include "cAbstCondMean.h"

namespace RegArchLib {
    class cAr : public cAbstCondMean
    {
    public:
        cAr(uint theNParams = 0);
        cAr(const cDVector& theParams);
        virtual ~cAr();
        double mComputeMean(const cRegArchValue & theData, int t) const;
    };
}

#endif /* CAR_H */

