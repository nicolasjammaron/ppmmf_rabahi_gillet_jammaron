/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   cRegArchModel.h
 * Author: rabahis
 *
 * Created on December 4, 2017, 11:55 AM
 */
#pragma once
#ifndef CREGARCHMODEL_H
#define CREGARCHMODEL_H

#include "StdAfxRegArchLib.h"

/*#include "cCondMean.h"
#include "cAbstCondVar.h"
#include "cAbstResiduals.h"
#include "cRegArchValue.h"*/

#include "cCondMean.h"
#include "cConstCondVar.h"
#include "cAbstResiduals.h"

namespace RegArchLib {
    class cRegArchModel
    {
    private :
        cCondMean *mvCondMean;
        cConstCondVar *mvCondVar;
        cAbstResiduals *mvResiduals;
    public:
        cRegArchModel(); //A default constructor
        cRegArchModel(cCondMean& theCondMean, cConstCondVar& theCondVar, cAbstResiduals& theResiduals);  // An other constructor
        ~cRegArchModel(); //A simple destructor
        cDVector mSimulate(cRegArchValue& theData, int theNbStep); 
        double mLikelyhood (cRegArchValue &theData, int thNbStep); 
        cRegArchModel* mCopy() const; //return a copy of the RegArchModel
    };
}


#endif /* CREGARCHMODEL_H */

