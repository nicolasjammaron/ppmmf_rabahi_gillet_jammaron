/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "cAbstCondMean.h"

namespace RegArchLib {
    
    cAbstCondMean::cAbstCondMean(uint theNParams) {
        mtNParams = theNParams;
        mtParams.ReAlloc(theNParams);
    }
    
    cAbstCondMean::cAbstCondMean(const cGSLVector& theParams) {
        mtNParams = theParams.GetSize();
        mtParams = theParams;
    }
    
    cAbstCondMean::~cAbstCondMean() {
        mtParams.Delete();
    }
    
    void cAbstCondMean::Delete(void) {
        mtParams.Delete();
    }
    
    void cAbstCondMean::ReAlloc(uint theSize) {
        mtNParams = theSize;
        mtParams.ReAlloc(theSize);
    }
    
    void cAbstCondMean::ReAlloc(const cGSLVector& theVectParam) {
        mtNParams = theVectParam.GetSize();
        mtParams = theVectParam;
    }
    
    double cAbstCondMean::Get(uint theIndex) {
        return mtParams[theIndex];
    }
    
    int cAbstCondMean::GetNParam(void) const {
        return mtNParams;
    }
    
    
    
    void cAbstCondMean::Set(const cDVector& theVectParam) {
        mtNParams = theVectParam.GetSize();
	mtParams = theVectParam;
    }
    
    void cAbstCondMean::Set(double theValue, uint theIndex) {
        if (theIndex >= mtParams.GetSize()) {
            throw cError("Bad index");
        }
        else {
            mtParams[theIndex] = theValue;
        }
    }
}