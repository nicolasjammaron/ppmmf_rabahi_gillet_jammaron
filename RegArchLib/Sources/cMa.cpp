
#include "cMa.h"

namespace RegArchLib{
    
    cMa::cMa(const cGSLVector& theParams) : cAbstCondMean(theParams){};
    
    cMa::cMa(uint theNParams) : cAbstCondMean( theNParams ){};
    
    cMa::~cMa(){
        
    };
    
    double cMa::mComputeMean(const cRegArchValue& theData, int t) const{
        double myMean = 0;       
        for (int j = 0; j < mtNParams; j++){
            if (t-j-1 >= 0) {
                myMean += mtParams[j] * theData.getUt()[t-j-1];
            }
        }
        return myMean;
    };
}