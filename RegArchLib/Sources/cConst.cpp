#include "cConst.h"

namespace RegArchLib {
    
    cConst::cConst(uint theNParams) : cAbstCondMean( theNParams ){};
    
    cConst::cConst(const cGSLVector& theParams) : cAbstCondMean(theParams){};
    
    cConst::~cConst(){
        
    };
    
    double cConst::mComputeMean(const cRegArchValue& theData, int t) const {
        return mtParams[0];
    };
    
    
    
}