/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "cRegArchValue.h"

namespace RegArchLib {

    cRegArchValue::cRegArchValue(uint theSize, cDMatrix* theXt){
       	if (theSize > 0)
	{	mvYt.ReAlloc(theSize) ;
		if ( theXt == NULL )
			mvXt.Delete() ;
		else
		{	if ( theXt->GetMat() == NULL )
				mvXt.Delete() ;
			else
				mvXt = *theXt ;
		}
		mvMt.ReAlloc(theSize) ;
		mvHt.ReAlloc(theSize) ;
		mvUt.ReAlloc(theSize) ;
		mvEpst.ReAlloc(theSize) ;
	}
	else
	{	mvYt.Delete();
		mvXt.Delete() ;
		mvMt.Delete() ;
		mvHt.Delete() ;
		mvUt.Delete() ;
		mvEpst.Delete() ;
	} 
       
    }

    cRegArchValue::cRegArchValue(const cDVector& theYt, const cDVector& theUt, const cDVector& theHt, const cDVector& theEpst, const cDVector& theMt, const cDMatrix& theXt){
        mvYt = theYt;
        mvUt = theUt;
        mvHt = theHt;
        mvEpst = theEpst;
        mvMt = theMt;
        mvXt = theXt;
    }
    
    cRegArchValue::~cRegArchValue(){
        mvYt.Delete();
        mvUt.Delete();
        mvHt.Delete();
        mvEpst.Delete();
        mvMt.Delete();
        mvXt.Delete();
    }
    
    cDVector cRegArchValue::getYt() const{
        return mvYt;
    }   
    
    cDVector cRegArchValue::getUt() const{
        return mvUt;
    }  
    
    cDVector cRegArchValue::getHt() const{
        return mvHt;
    }  
    
    cDVector cRegArchValue::getEpst() const{
        return mvEpst;
    }  
    
    cDVector cRegArchValue::getMt() const{
        return mvMt;
    }  
    
    double cRegArchValue::getYt(int theIndex) const{
        return mvYt[theIndex];
    }
    
    double cRegArchValue::getUt(int theIndex) const{
        return mvYt[theIndex];
    }
    
    double cRegArchValue::getHt(int theIndex) const{
        return mvYt[theIndex];
    }
    
    double cRegArchValue::getEpst(int theIndex) const{
        return mvYt[theIndex];
    }
    
    double cRegArchValue::getMt(int theIndex) const{
        return mvMt[theIndex];
    }
    
    void cRegArchValue::setYt(const cDVector& theYt){
        mvYt = theYt;
    }
    
    void cRegArchValue::setUt(const cDVector& theUt){
        mvUt = theUt;
    }
    
    void cRegArchValue::setHt(const cDVector& theHt){
        mvHt = theHt;
    }
    
    void cRegArchValue::setEpst(const cDVector& theEpst){
        mvEpst = theEpst;
    }
    
    void cRegArchValue::setMt(const cDVector& theMt){
        mvMt = theMt;
    }
    
    void cRegArchValue::setYt(double theValue, int theIndex){
        if (theIndex >= mvYt.GetSize())
			throw cError("Bad index") ;
		else
			mvYt[theIndex]=theValue ;
    }
    
    void cRegArchValue::setUt(double theValue, int theIndex){
        if (theIndex >= mvUt.GetSize())
			throw cError("Bad index") ;
		else
			mvUt[theIndex]=theValue ;
    }
    
    void cRegArchValue::setHt(double theValue, int theIndex){
        if (theIndex >= mvHt.GetSize())
			throw cError("Bad index") ;
		else
			mvHt[theIndex]=theValue ;
    }
    
    void cRegArchValue::setEpst(double theValue, int theIndex){
        if (theIndex >= mvEpst.GetSize())
			throw cError("Bad index") ;
		else
			mvEpst[theIndex]=theValue ;
    }
    
    void cRegArchValue::setMt(double theValue, int theIndex){
        if (theIndex >= mvMt.GetSize())
			throw cError("Bad index") ;
		else
			mvMt[theIndex]=theValue ;
    }
    
    /*!
     * \fn void cRegArchValue::ReAlloc(uint theSize)
     * \param uint theSize: new size of datas
    */
    void cRegArchValue::ReAlloc(uint theSize)
    {
	if (theSize > 0)
	{	
            if (mvYt.GetSize() != theSize)
		mvYt.ReAlloc(theSize) ;
		if (mvXt.GetNRow() != theSize)
		{	
                    if (mvXt.GetNCol() > 0)
			mvXt.ReAlloc(theSize, mvXt.GetNCol()) ;
		}
            if (mvMt.GetSize() != theSize)
                mvMt.ReAlloc(theSize) ;
            if (mvHt.GetSize() != theSize)
		mvHt.ReAlloc(theSize) ;
            if (mvUt.GetSize() != theSize)
		mvUt.ReAlloc(theSize) ;
            if (mvEpst.GetSize() != theSize)
		mvEpst.ReAlloc(theSize) ;
        }
	else
	{	
            mvYt.Delete() ;
            ClearMatrix(mvXt) ;
            mvMt.Delete() ;
            mvHt.Delete() ;
            mvUt.Delete() ;
            mvEpst.Delete() ;
	}
    }
    
}
