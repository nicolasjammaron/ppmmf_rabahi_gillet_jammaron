/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "cAbstResiduals.h"

namespace RegArchLib {
    
    cAbstResiduals::cAbstResiduals(int theNbParam){
        mtNbParam = theNbParam;
        mtParam.ReAlloc(theNbParam);
    }
    
    cAbstResiduals::cAbstResiduals(const cDVector & theParams){
        mtNbParam = theParams.GetSize();
        mtParam = theParams;
    }
        
    cAbstResiduals::~cAbstResiduals(){
        //mvParam.delete();
    }
    
    void cAbstResiduals::Set(int theIndex, double theValue){
       if (theIndex >= mtParam.GetSize())
			throw cError("Bad index") ;
		else
			mtParam[theIndex]=theValue ; 
    }
    
    void cAbstResiduals::Set(int theNbParam){
        mtNbParam = theNbParam;
    }
    
    void cAbstResiduals::Set(const cDVector & theParam){
        mtParam = theParam;
    }
    
    double cAbstResiduals::Get(int theIndex){
        if (theIndex >= mtParam.GetSize())
			throw cError("Bad index") ;
		else
                    return mtParam[theIndex];
    }
    
}
    
    
