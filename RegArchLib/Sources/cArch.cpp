/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "cArch.h"

namespace RegArchLib {
    
    cArch::cArch(const cGSLVector& theParams) : cAbstCondVar(theParams){};
    
    cArch::cArch(uint theNParams) : cAbstCondVar( theNParams ){};
    
    cArch::~cArch(){     
    };
    
    double cArch::mComputeVar(const cRegArchValue & theData, int t) const {
        double myVar = 0.;
        for(int j = 0; j < mtNParams; j++){
            if(t-j-1 >= 0){
                myVar += mtParams[j]* theData.getUt()[t-j-1]* theData.getUt()[t-j-1];
            }
        }
        return myVar;
    }
}