/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "cRegArchModel.h"

namespace RegArchLib{
    //TODO toutes les méthodes 
    cRegArchModel::cRegArchModel(){
        mvCondMean = NULL;
        mvCondVar = NULL;
        mvResiduals = NULL;
    }
    
    cRegArchModel::cRegArchModel(cCondMean& theCondMean, cConstCondVar& theCondVar, cAbstResiduals& theResiduals){
        mvCondMean = new cCondMean(theCondMean);
        mvCondVar = new cConstCondVar(theCondVar);
        //mvResiduals = theResiduals.PtrCopy();
    }
    
    cRegArchModel::~cRegArchModel(){
        //delete[] *mvResiduals;
    }
    
    cDVector cRegArchModel::mSimulate(cRegArchValue& theData, int theNbStep){
        theData.ReAlloc(theNbStep);
        for(int t=0; t < theNbStep; t++){
            theData.setHt(mvCondVar->mComputeVar(theData,t),t);
            theData.setEpst(mvResiduals->mSimulate(),t);
            theData.setMt(mvCondMean->mComputeMean(theData,t),t);
            theData.setYt(theData.getMt(t)+sqrt(theData.getHt(t))*theData.getHt(t));
            theData.setUt(sqrt(theData.getMt(t))*theData.getEpst(t));
        }
    }
    
    double cRegArchModel::mLikelyhood(cRegArchValue& theData, int theNbStep){
        double myLogLike = 0.0;
        for(int t = 0; t < theNbStep; t++){
            theData.setHt(mvCondVar->mComputeVar(theData, t), t);
            theData.setMt(mvCondMean->mComputeMean(theData, t), t);
            theData.setEpst((theData.getYt(t)-theData.getMt(t))/sqrt(theData.getHt(t)), t);
            theData.setUt(theData.getYt(t)-theData.getMt(t));
            
            myLogLike += -log(theData.getHt(t))/2 + mvResiduals->mLogDensity(theData.getEpst(t));
        }
        return myLogLike;
    }
    
    cRegArchModel* cRegArchModel::mCopy() const{
        cRegArchModel *theModel = new cRegArchModel();
        //theModel->mvCondMean = mvCondMean->copy();
        return theModel;
    }
    

}