/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "cGarch.h"

namespace RegArchLib {
    
    cGarch::cGarch(const cGSLVector& theParams) : cAbstCondVar(theParams){};
    
    cGarch::cGarch(uint theNParams) : cAbstCondVar( theNParams ){};
    
    cGarch::~cGarch(){     
    };
    
    double cGarch::mComputeVar(const cRegArchValue & theData, int t) const {
        double myVar = 0.;
        for(int j = 0; j < mtNParams; j++){
            if(t-j-1 >= 0){
                myVar += mtParams[j]* theData.getHt()[t-j-1];
            }
        }
        return myVar;
    }
}