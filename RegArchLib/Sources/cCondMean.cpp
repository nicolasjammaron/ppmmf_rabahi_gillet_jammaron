/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "cCondMean.h"

namespace RegArchLib {

    cCondMean::cCondMean(uint theNParams) {
        std::vector<cAbstCondMean *> tmp(theNParams);
        mvMeans = tmp;
    }
    
    cCondMean::cCondMean(const std::vector<cAbstCondMean *>& theMeans) {
        mvMeans = theMeans;
    }
    
    cCondMean::~cCondMean() {
        
    }
    
    cAbstCondMean * cCondMean::Get(uint theIndex ) const {
        return mvMeans[theIndex];
    }
    
    uint cCondMean::GetNMean(void) const {
        return mvMeans.size();
    }
    
    void cCondMean::Set(cAbstCondMean *theMean, uint theIndex) {
        if (theIndex >= mvMeans.size()) {
            throw cError("Bad index");
        }
        else {
            mvMeans[theIndex] = theMean;
        }
    }
    
    void cCondMean::Set(const std::vector<cAbstCondMean *>& theMeans) {
        mvMeans = theMeans;
    }
    
    double cCondMean::mComputeMean(const cRegArchValue& theData, int t) const {
        double myMean = 0;
        for (int i = 0; i < mvMeans.size(); i++) {
            myMean += this->Get(i)->mComputeMean(theData, t);
        }
        return myMean;
    }
    
}