/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "cNormResiduals.h"

namespace RegArchLib {
    
    cNormResiduals::cNormResiduals(int theNbParam) : cAbstResiduals(theNbParam){ }
    
    cNormResiduals::cNormResiduals(const cDVector& theParams) : cAbstResiduals(theParams){ }
    
    cNormResiduals::~cNormResiduals(){
        
    }
    
    double cNormResiduals::mLogDensity(double d){
        return 0.0;
    }
    
    double cNormResiduals::mSimulate(){
        return 0.0;
    }
}