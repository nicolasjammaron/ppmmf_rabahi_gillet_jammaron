/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "cAbstCondVar.h"

namespace RegArchLib {
    
    cAbstCondVar::cAbstCondVar(uint theNParams) {
        mtNParams = theNParams;
        mtParams.ReAlloc(theNParams);
    }
    
    cAbstCondVar::cAbstCondVar(const cGSLVector& theParams) {
        mtNParams = theParams.GetSize();
        mtParams = theParams;
    }
    
    cAbstCondVar::~cAbstCondVar() {
        mtParams.Delete();
    }
    
}