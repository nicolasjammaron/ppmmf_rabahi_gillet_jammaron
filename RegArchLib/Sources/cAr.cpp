#include "cAr.h"

namespace RegArchLib {
    
    cAr::cAr(uint theNParams) : cAbstCondMean(theNParams){};
    
    cAr::cAr(const cGSLVector& theParams) : cAbstCondMean(theParams){}; 
    
    cAr::~cAr(){
        
    };
    
    double cAr::mComputeMean(const cRegArchValue & theData, int t) const{
        double myMean = 0;
        for (int i = 0; i < mtNParams; i++){
            if (t-i-1 >= 0) {
                myMean += mtParams[i] * theData.getYt(t-i-1);
            }
        }
        return myMean;
    };
    
   
}