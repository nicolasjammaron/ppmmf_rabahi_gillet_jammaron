/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "cConstCondVar.h"

namespace RegArchLib {
    
    cConstCondVar::cConstCondVar(uint theNParams) : cAbstCondVar( theNParams ){};
    
    cConstCondVar::cConstCondVar(const cGSLVector& theParams) : cAbstCondVar(theParams){};
    
    cConstCondVar::~cConstCondVar(){
        
    };
    
    double cConstCondVar::mComputeVar(const cRegArchValue& theData, int t) const {
        return mtParams[0];
    };
    
}